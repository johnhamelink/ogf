# Our Green Future [![Netlify Status](https://api.netlify.com/api/v1/badges/e1c0e677-b045-48a6-8897-d844784e4910/deploy-status)](https://app.netlify.com/sites/ourgreenfuture/deploys)

<img src="./res/img/logo_small.png" alt="Our Green Future Logo" width="250"/>

 - [Netlify](https://app.netlify.com/sites/ourgreenfuture/overview)
 - [Production URL](https://ourgreenfuture.scot)
